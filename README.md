# Jenkinsを設定込みでGit管理+Docker起動させる

## 事前準備

鍵作成
```
$ ssh-keygen -t rsa -f id_rsa -N '' && \
  mv id_rsa master/ && \
  echo "JENKINS_SLAVE_SSH_PUBKEY=`cat id_rsa.pub`" > .env
```

## 起動方法

docker.sockのpermmisonを確認
```
$ sudo ls -l /var/run/docker.sock
srw-rw---- 1 root docker 0 Oct  4 07:50 /var/run/docker.sock
// rootグループでない場合はrootに変更しておく
$ sudo chown root:root /var/run/docker.sock
$ sudo ls -l /var/run/docker.sock
srw-rw---- 1 root root 0 Oct  4 07:50 /var/run/docker.sock
```

イメージビルドして起動
```
$ sudo docker-compose build
$ sudo docker-compose up
```

ちょっと待ってアクセス
ログイン後に,Jenkins設定のJenkinsの位置→Jenkins URLを変更しておく
ジョブのtest-docker-2が動けばおk


## 設定をgitで管理する

[参考](https://github.com/sue445/jenkins-backup-script)

## 要注意点

* Jenkins設定のJenkinsの位置→Jenkins URLがビルド元に依存するので検討
  * 外部から環境変数で注入とか `jenkins.model.JenkinsLocationConfiguration.xml` にあるので
* Dockerを使うときにdocker.sockをvolume接続して使っている. Permissionの問題でjenkinsユーザーのグループをdocker.sockとあわせてやるといいけどrootグループにした方がいい(DockerホストとDockerコンテナ内でgroupidとグループ名の対応づけが異なるため、ホストではdockerグループなのに、コンテナから見るとpingグループとかになる.. rootなら必ずgid=0になるので)

